## Deploy Portainer to swarm bastion

```
sudo usermod -aG docker $(whoami)
sudo docker swarm init
sudo docker stack deploy -c docker-compose.yml portainer
```

## Portainer forwarding to http://localhost:9000 over ssh
ssh -i ~/.docker/machine/machines/swarm-bastion/id_rsa -L 9000:localhost:9000 docker-user@10.10.10.10
#!/bin/bash
set -x
# shellcheck disable=SC2120

############## SSH forward portainer to localhost ###################
# ssh -i ~/.docker/machine/machines/swarm-bastion/id_rsa -L 9000:localhost:9000 docker-user@10.10.10.10

STACK_NAME=portainer-deploy-test
DOCKER_STACK_YAML_FILE=docker-compose.yml

# ENDPOINT_ID=2 # Need if Portainer has more than one endpoint

source ./.env

function auth_toket() {
    http POST :9000/api/auth Username="$PORTAINER_USER" Password="$PORTAINER_PASS" |
        jq --raw-output .jwt

}

# https://devhints.io/httpie

function check_dependencies() {
    if ! command -v http >/dev/null 2>&1; then
        echo "\033[0;31m httpie not installed\033[0m

        This script requarement from httpie

        For install:
    sudo apt install httpie -y

    brew install httpie

    sudo dnf install httpie -y
    "
        exit 1
    fi
    if ! command -v jq >/dev/null 2>&1; then
        echo "\033[0;31m httpie not installed\033[0m

        This script requarement from jq

        For install:
    sudo apt install jq -y

    brew install jq

    sudo dnf install jq -y
    "
        exit 1
    fi
}

function get_stack_list() {
    local STACK_NAME=${STACK_NAME:-portainer-deploy-test}

    http GET ':9000/api/stacks' \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN"
}

function get_stack_id_by_name() {
    ##################### example #####################
    # get_stack_id_by_name portainer-deploy-test

    local STACK_NAME=$1
    local STACK_NAME=${STACK_NAME:-portainer-deploy-test}
    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http GET ':9000/api/stacks' \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN" |
        jq ".[] | select(.Name==\"$STACK_NAME\") | .Id"
}

function get_stack_file_by_name() {
    ##################### example #####################
    # get_stack_file_by_name portainer-deploy-test

    local STACK_NAME=${STACK_NAME:-"$1"}
    local STACK_NAME=${STACK_NAME:-portainer-deploy-test}
    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http GET ':9000/api/stacks/3/file' \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN"

}

function update_stack() {
    ##################### example #####################
    # update_stack portainer-deploy-test docker-stack.yml

    local STACK_NAME=${STACK_NAME:-"$1"}
    local STACK_NAME=${STACK_NAME:-portainer-deploy-test}

    local DOCKER_STACK_YAML_FILE=${DOCKER_STACK_YAML_FILE:-"$2"}
    local DOCKER_STACK_YAML_FILE=${DOCKER_STACK_YAML_FILE:-'docker-stack.yml'}

    local PORTAINER_ENDPOINT="$(select_endpoint_id)"

    local PORTAINER_STACK_ID=${PORTAINER_STACK_ID:-$(get_stack_id_by_name "$STACK_NAME")}
    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http PUT ":9000/api/stacks/${PORTAINER_STACK_ID}
        endpointId==$PORTAINER_ENDPOINT" \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN" \
        id=$PORTAINER_STACK_ID \
        StackFileContent=@"$DOCKER_STACK_YAML_FILE"
}

function get_endpoint_list() {
    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http GET ':9000/api/endpoints' \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN"

}

function get_endpoints_ids() {
    get_endpoint_list |
        jq ".[] | .Id"

}

function select_endpoint_id() {
    if [ -z "$ENDPOINT_ID" ]; then
        local ENDPOINT_ID_LIST=$(get_endpoints_ids)
        local ENDPOINT_ID_LIST_SIZE=$(echo "$ENDPOINT_ID_LIST" | wc -w)
        if [ "$ENDPOINT_ID_LIST_SIZE" -ne 1 ]; then
            echo -e "You have more, then one Portainer endporints. \nPlease set variable ENDPOINT_ID"
        else
            echo 1
        fi
    fi
}

function add_secret() {
    ##################### example #####################
    # add_secret secret_file

    local DOCKER_SECRET_FILE=${DOCKER_SECRET_FILE:-"$1"}
    local DOCKER_SECRET_FILE=${DOCKER_SECRET_FILE:-'docker-stack.yml'}

    local PORTAINER_ENDPOINT="$(select_endpoint_id)"

    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http -v POST ":9000/api/endpoints/${PORTAINER_ENDPOINT}/docker/secrets/create" \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN" \
        Name="$(basename $DOCKER_SECRET_FILE)" \
        Data="$(base64 $DOCKER_SECRET_FILE)"
}

add_config() {
    ##################### example #####################
    # add_config config_file

    local DOCKER_CONFIG_FILE=${DOCKER_CONFIG_FILE:-"$1"}
    local DOCKER_CONFIG_FILE=${DOCKER_CONFIG_FILE:-'docker-stack.yml'}

    local PORTAINER_ENDPOINT="$(select_endpoint_id)"

    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http -v POST ":9000/api/endpoints/${PORTAINER_ENDPOINT}/docker/configs/create" \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN" \
        Name="$(basename $DOCKER_CONFIG_FILE)" \
        Data="$(base64 $DOCKER_CONFIG_FILE)"
}

function get_endpoint_swarm_id(){
    local PORTAINER_ENDPOINT="$(select_endpoint_id)"
    local PORTAINER_OATH_TOKEN=${PORTAINER_OATH_TOKEN:-$(auth_toket)}

    http GET ":9000/api/endpoints/${PORTAINER_ENDPOINT}" \
        "Authorization: Bearer $PORTAINER_OATH_TOKEN" \
        | jq --raw-output '.Snapshots[].SnapshotRaw.Info.Swarm.Cluster.ID'

}

main() {
    check_dependencies
    PORTAINER_OATH_TOKEN="$(auth_toket)"
}
main
